Source: tomb
Section: utils
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: ChangZhuo Chen (陳昌倬) <czchen@debian.org>,
           Sven Geuer <sge@debian.org>,
Build-Depends: debhelper-compat (= 13),
               libgcrypt20-dev,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://dyne.org/tomb/
Vcs-Git: https://salsa.debian.org/pkg-security-team/tomb.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/tomb

Package: tomb
Architecture: any
Depends: cryptsetup-bin,
         e2fsprogs,
         file,
         gettext-base,
         gnupg,
         pinentry-curses | pinentry,
# TODO: python3 can get removed as soon as python3-minimal becomes Essential.
# Technically python3-minimal would suffice here, alas it is not allowed as
# a dependency. Call 'lintian-explain-tags depends-on-python-minimal' for
# further details.
         python3,
         sudo | doas,
         zsh,
         ${misc:Depends},
         ${shlibs:Depends},
Recommends: lsof,
Suggests: argon2,
          btrfs-progs,
          dcfldd,
          plocate,
          pwdsphinx,
          qrencode,
          recoll,
          steghide,
          unoconv,
Description: crypto undertaker
 Tomb is a free and easy to operate desktop application for fairly strong
 encryption of personal files. A tomb is like a locked folder that can be
 transported and hidden in filesystems; its keys are password protected and can
 be kept separate, for instance keeping the tomb file in your computer's
 harddisk and the key file on a USB stick.
 .
 Tomb relies on dm-crypt (and cryptsetup) as an encryption backend using the
 aes-xts-plain64 cypher.
